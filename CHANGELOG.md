# Changelog

All notable changes to this project will be documented in this file.

## Version 1.4.1 - 2020-07-16

### Fixed

* Fixed errors when exporting SSH and PGP keys

## Version 1.4.0 - 2020-06-17

### Added

* Rename a group
* Delete an account home directory when deleting the user

### Changes

* Update Faris.GnuSystem to 1.3.0

### Fixes

* Minor bugs
* Polkit not working on Ubuntu
* About dialog not displayed correctly the second time

## Version 1.3.0 - 2020-05-31

### Added

* You can now create system accounts
* "/usr/sbin/nologin" to Shells ComboBox
* Message dialog when deleting an account
* SystemUser and SystemGroup models
* Shells Users and Groups file parsers
* User and groups services

### Changes

* Added Gui Base classes to Faris.GnuSystem Library
* Glade files are now in Assets/UI directory
* Fixed bugs reading settings file
* Fixed bugs related to System Account creation
* Changing password and exporting user ssh, gpg and profile files not allowed for system account

## Version 1.2.2 - 2020-05-30

### Fixed

* Fixed Logs not showing when opening Logs Window

### Changes

* Logs are now saved in user's home directory

## Version 1.2.1 - 2020-05-29

### Fixed

* Bug when writing settings file

### Changes

* settings file is now created on .config user home directory

## Version 1.2.0 - 2020-05-29

### Added

* Dependency Injection for Gtk Windows
* Gtk Windows are now hidden instead of destroyed
* Popover on main window
* Settings Window
* Logs Window

### Changed

* Improve logging errors
* Better exception handling
* Added About Dialog in Main Popover

## Version 1.1.0 - 2020-05-15

### Added

* Building deb packages

### Changes

* Minor bugs fixes

### Removed

* Manually installing application from bash scripts

## Version 1.0.0 - 2020-05-09

### Added

* Logging
* Dependency Injection
* Delete Group Window
* About Dialog

### Changed

* Using Generic Host Builder
* Shell entry to Shell Combobox
* Main Window display list of users and groups
* Disable single file publishing
* Changed Main Window Layout

### Removed

* Faris. GuiApplication dependency

### Fixed

* Bug when searching a group name

## Version 0.0.4 - 2020-05-06

### Changed

* Added confirmation dialog when deleting a user.
* Updated Faris. GuiApplication to version 0.0.2

## Version 0.0.3 - 2020-05-03

### Added

* Add or remove user's groups.
* Delete user account.

### Fixed

* Bug when changing user password.

## Version 0.0.2 - 2020-05-04

### Added

* TreeView for displaying user's groups.
* Apply button when changing user's groups.

## Version 0.0.1 - 2020-05-03

### Added

* Displaying user acccounts.
* Create new user account.
* Modify user account.
* Create new group. 
