# Faris System Accounts
Faris System Account is a GTK application for managing GNU/Linux user accounts.
 
## Dependencies

- [.NET Core](https://dotnet.microsoft.com/download): .NET Core SDK for development
- [GTKSharp](https://github.com/GtkSharp/GtkSharp): C# GTK Bindings
- [Faris.System](https://gitlab.com/farisx/faris-system): Faris System Library

## Licence

This program is a Free/Libre software under GPLv3. See [COPYING](COPYING).

## Changelogs

See [CHANGELOG.md](CHANGELOG.md).

### How to install

You can download the debian package version you want from 
* [Gitlab Releases](https://gitlab.com/farisx/faris-system-accounts/-/releases)
* [Officiel Website](https://faris.ma/faris-system-accounts)
