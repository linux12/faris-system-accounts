#!/bin/bash
# THIS FILE IS USED BY GITLAB CI/CD.
# YOU CAN EXECUTE MANUALLY FOR LOCAL DEVELOPMENT

if [ -z "$1" ]; then
    echo "Error: You must specify application version"
    echo "Example: ./build-release.sh v1.1.3"
    exit 1
fi

PUBLISH_DIR="publish-output"
DEPLOY_DIR="deploy"
APP_NAME="faris-system-accounts_"
APP_VERSION=$1
DEBFILE_NAME=$APP_NAME$APP_VERSION

mkdir $PUBLISH_DIR $DEPLOY_DIR $DEBFILE_NAME
 
dotnet publish -c Release -r linux-x64 -o $PUBLISH_DIR
  
cp -R deb-pkg-files/* $DEBFILE_NAME/

mkdir -p $DEBFILE_NAME/opt/faris/system-accounts/

cp -R $PUBLISH_DIR/* $DEBFILE_NAME/opt/faris/system-accounts/

dpkg-deb --build $DEBFILE_NAME

sha256sum $DEBFILE_NAME.deb > $DEPLOY_DIR/$DEBFILE_NAME.deb.checksum 
mv $DEBFILE_NAME.deb $DEPLOY_DIR

 
rm -rf $PUBLISH_DIR
rm -rf $DEBFILE_NAME

exit 0