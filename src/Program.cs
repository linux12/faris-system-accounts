﻿//     This file is part of Faris System Accounts.
//     Faris System Accounts is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.

//     Faris System Accounts is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU General Public License for more details.

//     You should have received a copy of the GNU General Public License
//     along with Faris System Accounts.  If not, see<https://www.gnu.org/licenses/>.

using System; 
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Faris.SystemAccounts.App.Windows;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Faris.GnuSystem.Logging;
using Faris.SystemAccounts.App.Services; 

namespace Faris.SystemAccounts.App
{

    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            // var host = CreateHostBuilder(args).Build(); 

            // ApplicationStartup.Initialize(host);

            // var app = host.Services.GetService<ApplicationStartup<SystemAccountsApplication>>();

            // app.Start("");

            using (var app = new SystemAccountsApplication())
            {
                app.HostBuilder.ConfigureServices((hostingContext, services) =>
                {
                    services.AddLogging(options =>
                    {
                        options.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                    });
                    services.AddSingleton(typeof(ISystemAccountService), typeof(SystemAccountService));
                    services.AddSingleton(typeof(ISystemUserService), typeof(SystemUserService));
                    services.AddSingleton(typeof(ISystemGroupService), typeof(SystemGroupService));
                    services.AddSingleton(typeof(IUserProfileService), typeof(UserProfileService));

                }).ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.SetBasePath($"{Directory.GetCurrentDirectory()}");
                    config.AddJsonFile("appsettings.json");
                })
                .ConfigureLogging((hostContext, logging) =>
                    {
#if DEBUG
                        logging.AddDebug();
#endif
                        logging.AddFile(options =>
                        {
                            options.FileName = "log";
                            options.LogDirectory = $"/home/{Environment.UserName}/.config/faris/system-accounts/Logs";
                            options.FileSizeLimit = 5 * 1024 * 1024;
                            options.Extension = "txt";
                        });
                    });

                app.Run(typeof(MainWindow));

            }
        }
    }
}
