//     This file is part of Faris System Accounts.
//     Faris System Accounts is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.

//     Faris System Accounts is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU General Public License for more details.

//     You should have received a copy of the GNU General Public License
//     along with Faris System Accounts.  If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Faris.GnuSystem.Parsers;
using Faris.SystemAccounts.App.Models;

namespace Faris.SystemAccounts.App.Parsers {
    public class UsersParser : BaseParser<IList<SystemUser>> {
        public UsersParser () { }

        public override async Task<IList<SystemUser>> ParseContentAsync (string fileName) {
            var fileContent = await ReadFileContentAsync (fileName);
            return ParseUsersContent (fileContent);
        }
        private List<SystemUser> ParseUsersContent (string content) {
            if (string.IsNullOrEmpty (content))
                throw new ArgumentNullException (nameof (content));

            string[] lines = content.Split ('\n');
            List<SystemUser> users = new List<SystemUser> ();

            foreach (var line in lines) {
                var user = ParseContentLine (line);
                if (user != null)
                    users.Add (user);
            }
            return users;
        }
        private SystemUser ParseContentLine (string line) {
            if (!string.IsNullOrEmpty (line)) {
                SystemUser user = new SystemUser ();

                string[] systemUser = line.Split (':');
                string[] systemUserInfo = string.IsNullOrEmpty (systemUser[4]) ? null : systemUser[4].Split (',');

                user.Login = systemUser[0];
                user.Name = systemUserInfo != null ? systemUserInfo[0] : string.Empty;

                user.UserID = int.Parse (systemUser[2]);
                user.GroupID = int.Parse (systemUser[3]);

                user.Home = systemUser[5];
                user.Shell = systemUser[6];

                if (user.Shell.Equals ("/bin/false") || user.Shell.Equals ("/usr/sbin/nologin")) {
                    user.IsSystem = true;
                    if (!user.Home.Equals ("/nonexistent")) {
                        user.IsSystemHome = true;
                    }
                }
                return user;
            }
            return null;
        }
    }
}