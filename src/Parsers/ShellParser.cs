//     This file is part of Faris System Accounts.
//     Faris System Accounts is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.

//     Faris System Accounts is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU General Public License for more details.

//     You should have received a copy of the GNU General Public License
//     along with Faris System Accounts.  If not, see<https://www.gnu.org/licenses/>.
 
using System.Collections.Generic;
using System.Threading.Tasks;
using Faris.GnuSystem.Parsers;

namespace Faris.SystemAccounts.App.Parsers
{
    public class ShellParser: BaseParser<List<string>>
    {
        public override async Task<List<string>> ParseContentAsync(string fileName)
        {
            var lstShells = new List<string>();
            var result = await base.ReadFileContentAsync(fileName);

            foreach (var line in result.Split('\n'))
            {
                if (!line.StartsWith('#') && line.StartsWith('/'))
                {
                    lstShells.Add(line.Trim());
                }
            }
            return lstShells;
        }
    }
}
