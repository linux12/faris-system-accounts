//     This file is part of Faris System Accounts.
//     Faris System Accounts is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.

//     Faris System Accounts is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU General Public License for more details.

//     You should have received a copy of the GNU General Public License
//     along with Faris System Accounts.  If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Faris.GnuSystem.Parsers;
using Faris.SystemAccounts.App.Models;

namespace Faris.SystemAccounts.App.Parsers {
    public class GroupsParser : BaseParser<IList<SystemGroup>> {
        public GroupsParser () {

        }

        public override async Task<IList<SystemGroup>> ParseContentAsync (string fileName) {
            var fileContent = await ReadFileContentAsync (fileName);
            return ParseGroupsContent (fileContent);
        }

        private List<SystemGroup> ParseGroupsContent (string content) {
            if (string.IsNullOrEmpty (content))
                throw new ArgumentNullException (nameof (content));

            string[] lines = content.Split ('\n');
            List<SystemGroup> groups = new List<SystemGroup> ();

            foreach (var line in lines) {
                var user = ParseContentLine (line);
                if (user != null)
                    groups.Add (user);
            }
            return groups;
        }
        private SystemGroup ParseContentLine (string line) {
            if (!string.IsNullOrEmpty (line)) {
                SystemGroup group = new SystemGroup ();

                string[] systemGroup = line.Split (':');

                group.Name = systemGroup[0];
                group.ID = int.Parse (systemGroup[2]);

                return group;
            }
            return null;
        }
    }
}