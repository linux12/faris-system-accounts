//     This file is part of Faris System Accounts.
//     Faris System Accounts is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.

//     Faris System Accounts is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU General Public License for more details.

//     You should have received a copy of the GNU General Public License
//     along with Faris System Accounts.  If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faris.GnuSystem.Commands;
using Faris.GnuSystem.Exceptions;
using Faris.GnuSystem.Gui.Controls.DataGrid;
using Faris.GnuSystem.Gui.Windows;
using Faris.SystemAccounts.App.Models;
using Faris.SystemAccounts.App.Services;
using Microsoft.Extensions.Logging;

namespace Faris.SystemAccounts.App.Windows {
    public class UserWindow : Window {
        Gtk.ScrolledWindow groupScrollWindow;
        Gtk.Entry groupSearchEntry;
        Gtk.ComboBoxText shellsComboBox;
        Gtk.Entry nameEntry;
        Gtk.Entry homeEntry;
        Gtk.Entry loginEntry;
        Gtk.Entry passwordEntry;
        Gtk.Entry confirmPasswordEntry;
        Gtk.Button changePasswordButton;
        Gtk.Button exportButton;
        Gtk.Popover exportPopover;
        Gtk.Button exportProfileButton;
        Gtk.Button exportPgpButton;
        Gtk.Button exportSshButton;
        Gtk.Button applyGroupsButton;
        Gtk.Button deleteUserButton;
        Gtk.Button changeUserButton;
        Gtk.CheckButton deleteHomeCheckbox;
        IList<string> _shells;
        List<GroupModel> _currentUserGroups;
        SystemUser _selectedUser;
        SystemUser _modifedUser;
        DataGridControl<GroupModel> _groupsGrid;
        readonly ILogger<UserWindow> _logger;
        readonly ISystemAccountService _systemAccountService;
        readonly IUserProfileService _userProfileService;
        public UserWindow (ISystemAccountService systemAcccountService,
            IUserProfileService userProfileService, ILogger<UserWindow> logger) {
            _systemAccountService = systemAcccountService;
            _modifedUser = new SystemUser ();
            _logger = logger;
            _currentUserGroups = new List<GroupModel> ();
            _userProfileService = userProfileService;
        }
        public override Task NavigatingFromAsync () {
            _logger.LogTrace ("Navigated From");
            return Task.CompletedTask;
        }

        public override async Task NavigatingToAsync () {
            _logger.LogTrace ("Navigated To");

            _selectedUser = InputParameter as SystemUser;

            GetNativeObjects ();

            _groupsGrid = new DataGridControl<GroupModel> (groupScrollWindow, groupSearchEntry);
            _groupsGrid.AddColumn (new TextColumn ("Group ID", "ID"));
            _groupsGrid.AddColumn (new TextColumn ("Group Name", "Name", true));

            var toggleColumn = new ToggleColumn ("Selected", "Selected");

            toggleColumn.ToggleChanged += (sender, args) => {
                var groupModel = args.Model as GroupModel;
                if (args.ToggleValue) {
                    _currentUserGroups.Add (groupModel);
                } else {
                    _currentUserGroups.Remove (groupModel);
                }
            };

            _groupsGrid.AddColumn (toggleColumn);

            _groupsGrid.Show ();

            await PopulateShellsComboBox ();

            await PopulateUserGroups ();

            DisplayUserInfo ();
        }

        private async void OnSshExportClicked (object sender, EventArgs e) {
            var result = LaunchFolderChooser ("Select a directory to export GnuPG keys");
            if (result.Response != Gtk.ResponseType.Accept) {
                return;
            }
            try {
                await _userProfileService.ExportSshKeysAsync (_selectedUser, result.SelectedFolder);
            } catch (Exception ex) {
                _logger.LogError (ex, "Error exporting user's ssh keys");
                ShowMessageDialog (Gtk.MessageType.Error, Gtk.ButtonsType.Ok, "Error exporting user's ssh keys");
            }
        }

        private async void OnProfileExportClicked (object sender, EventArgs e) {

            var result = LaunchFolderChooser ("Select a directory to export GnuPG keys");
            if (result.Response != Gtk.ResponseType.Accept) {
                return;
            }
            try {
                await _userProfileService.ExportBashFilesAsync (_selectedUser, result.SelectedFolder);
            } catch (Exception ex) {
                _logger.LogError (ex, "Error exporting bash script files");
                ShowMessageDialog (Gtk.MessageType.Error, Gtk.ButtonsType.Ok, "Error exporting user's profiles files");
            }
        }

        private async void OnPgpExportClicked (object sender, EventArgs e) {
            var result = LaunchFolderChooser ("Select a directory to export GnuPG keys");
            if (result.Response != Gtk.ResponseType.Accept) {
                return;
            }
            try {
                await _userProfileService.ExportPgpKeysAsync (_selectedUser, result.SelectedFolder);
            } catch (Exception ex) {
                _logger.LogError (ex, "Error exporting user's gnupg keys");
                ShowMessageDialog (Gtk.MessageType.Error, Gtk.ButtonsType.Ok, "Error exporting user's gnupg keys");
            }
        }

        private void GetNativeObjects () {
            groupScrollWindow = GetNativeObject<Gtk.ScrolledWindow> ("_groupScrolledWindow");
            groupSearchEntry = GetNativeObject<Gtk.Entry> ("_searchGroupNameEntry");
            shellsComboBox = GetNativeObject<Gtk.ComboBoxText> ("_shellsComboBoxText");
            nameEntry = GetNativeObject<Gtk.Entry> ("_nameEntry");
            homeEntry = GetNativeObject<Gtk.Entry> ("_homeEntry");
            loginEntry = GetNativeObject<Gtk.Entry> ("_loginEntry");
            passwordEntry = GetNativeObject<Gtk.Entry> ("_passwordEntry");
            confirmPasswordEntry = GetNativeObject<Gtk.Entry> ("_confirmPasswordEntry");
            changePasswordButton = GetNativeObject<Gtk.Button> ("_changePasswordButton");
            exportButton = GetNativeObject<Gtk.Button> ("_exportButton");
            exportPopover = GetNativeObject<Gtk.Popover> ("_exportPopover");
            exportProfileButton = GetNativeObject<Gtk.Button> ("_exportProfileButton");
            exportPgpButton = GetNativeObject<Gtk.Button> ("_exportPgpButton");
            exportSshButton = GetNativeObject<Gtk.Button> ("_exportSshButton");
            applyGroupsButton = GetNativeObject<Gtk.Button> ("_applyGroupsButton");
            deleteUserButton = GetNativeObject<Gtk.Button> ("_deleteUserButton");
            changeUserButton = GetNativeObject<Gtk.Button> ("_changeUserButton");
            deleteHomeCheckbox = GetNativeObject<Gtk.CheckButton> ("_deleteHomeDirectoryCheckbox");
        }

        private async Task ApplyUserGroups () {
            try {
                await _systemAccountService.SetUserGroupsAsync (_selectedUser, _currentUserGroups.Select (x => x.Name).ToList ());
                ShowMessageDialog (Gtk.MessageType.Info, Gtk.ButtonsType.Ok, $"Groups applied successfully for user {_selectedUser.Login}");
            } catch (System.Exception ex) {
                _logger.LogError (ex, $"Error setting groups for user {_selectedUser.Login}");
            }
        }

        private async Task PopulateShellsComboBox () {
            _shells = new List<string> (await _systemAccountService.GetSystemShellsAsync ());

            foreach (var item in _shells) {
                shellsComboBox.AppendText (item);
            }
        }
        private async Task PopulateUserGroups () {
            var userGroups = await _systemAccountService.GetUserGroups (_selectedUser.Login);
            var groups = await _systemAccountService.GetGroupsAsync ();
            var orderedGroups = groups.OrderByDescending (x => x.ID);
            _currentUserGroups = new List<GroupModel> ();
            var groupModels = new List<GroupModel> ();
            foreach (var item in orderedGroups) {
                var group = new GroupModel () {
                    ID = item.ID,
                    Name = item.Name,
                    Selected = userGroups.Contains (item.Name)
                };
                if (group.Selected) {
                    _currentUserGroups.Add (group);
                }
                groupModels.Add (group);
            }
            _groupsGrid.Populate (groupModels);
        }

        private void DisplayUserInfo () {
            var user = InputParameter as SystemUser;

            _selectedUser = _systemAccountService.Users.Where (x => x.UserID == user.UserID).FirstOrDefault ();

            if (_selectedUser != null) {
                changePasswordButton.Clicked += async (sender, args) => { await UpdateUserAccountPassword (); };
                changeUserButton.Clicked += async (sender, args) => { await UpdateAccountInfoAsync (); };
                nameEntry.Changed += (sender, args) => { _modifedUser.Name = nameEntry.Text; };
                loginEntry.Changed += (sender, args) => { _modifedUser.Login = loginEntry.Text; };
                homeEntry.Changed += (sender, args) => { _modifedUser.Home = homeEntry.Text; };
                shellsComboBox.Changed += (sender, args) => { _modifedUser.Shell = shellsComboBox.ActiveText; };
                exportButton.Clicked += (sender, args) => {
                    exportPopover.ShowAll ();
                    exportPopover.Popup ();
                };
                exportPgpButton.Clicked += OnPgpExportClicked;
                exportProfileButton.Clicked += OnProfileExportClicked;
                exportSshButton.Clicked += OnSshExportClicked;
                applyGroupsButton.Clicked += async (sender, args) => { await ApplyUserGroups (); };
                deleteUserButton.Clicked += async (sender, args) => { await DeleteUserAsync (); };

                _modifedUser.UserID = _selectedUser.UserID;
                _modifedUser.GroupID = _selectedUser.GroupID;
                nameEntry.Text = _selectedUser.Name;
                loginEntry.Text = _selectedUser.Login;
                homeEntry.Text = _selectedUser.Home;

                var shellIndex = _shells.IndexOf (_selectedUser.Shell);
                shellsComboBox.Active = shellIndex;

                if (_selectedUser.IsSystem) {
                    if (!_selectedUser.IsSystemHome) {
                        homeEntry.Parent.Visible = false;
                    }
                    //where not handling system accounts exports and password changes
                    exportButton.Visible = false;
                    shellsComboBox.Parent.Visible = false;
                    passwordEntry.Parent.Visible = false;
                    confirmPasswordEntry.Parent.Visible = false;
                    changePasswordButton.Visible = false;
                }
            }
        }
        private async Task UpdateUserAccountPassword () {
            if (!passwordEntry.Text.Equals (confirmPasswordEntry.Text)) {
                ShowMessageDialog (Gtk.MessageType.Error, Gtk.ButtonsType.Ok, "Error passwords are not the same.");
                return;
            }

            try {
                var changed = await _systemAccountService.ChangePasswordAsync (_selectedUser.Login, confirmPasswordEntry.Text);
                if (changed) {
                    confirmPasswordEntry.Text = string.Empty;
                    passwordEntry.Text = string.Empty;
                    ShowMessageDialog (Gtk.MessageType.Info, Gtk.ButtonsType.Ok, "Password changed successfully.");
                } else {
                    _logger.LogError ($"Error changing password for user {_selectedUser.Login}");
                }
            } catch (SystemCommandException exc) {
                ShowMessageDialog (Gtk.MessageType.Error, Gtk.ButtonsType.Ok, "Error changing password.");

                _logger.LogError (exc, $"Error changing password for user {_selectedUser.Login}");
            }
        }
        private async Task UpdateAccountInfoAsync () {
            try {
                bool changed = await _systemAccountService.ChangeUserAsync (_selectedUser, _modifedUser);
                if (changed) {
                    OutputParameter = true;
                    ShowMessageDialog (Gtk.MessageType.Info, Gtk.ButtonsType.Ok, "User account information changed successfully");
                } else {
                    _logger.LogError ($"Error updating user account information");
                }
            } catch (SystemCommandException ex) {
                _logger.LogError (ex, $"Error updating user account information");
            }
        }
        private async Task DeleteUserAsync () {
            var dialogMessage = $"Are you sure you want to delete user {_selectedUser.Login}";
            if (deleteHomeCheckbox.Active) {
                dialogMessage += " and his home directory";
            }

            if (ShowMessageDialog (Gtk.MessageType.Question, Gtk.ButtonsType.YesNo, dialogMessage) == Gtk.ResponseType.Yes) {
                try {
                    if (await _systemAccountService.DeleteUserAsync (_selectedUser)) {
                        OutputParameter = true;
                        if (deleteHomeCheckbox.Active)
                            await DeleteUserHomeDirectoryAsync ();
                        ShowMessageDialog (Gtk.MessageType.Info, Gtk.ButtonsType.Ok, $"User {_selectedUser.Login} deleted successfully.");
                        this.Close ();

                    }
                } catch (Exception exc) {
                    _logger.LogError (exc, $"Error deleting user {_selectedUser.Login}");
                }

            }
        }
        private async Task DeleteUserHomeDirectoryAsync () {

            ISystemCommand rmCmd = new SystemCommand ("rm");
            rmCmd.AppendArgument ("-rf");
            rmCmd.AppendArgument (_selectedUser.Home);

            if (!string.IsNullOrEmpty (_selectedUser.Home)) {
                if (ShowMessageDialog (Gtk.MessageType.Warning, Gtk.ButtonsType.YesNo,
                        $"Your about to permanently delete \"{_selectedUser.Home}\" directory, are you sur you want to proceed ?") == Gtk.ResponseType.Yes) {
                    try {
                        var result = await rmCmd.ExecuteAsync (true);
                        if (result.ExitCode == 0)
                            _logger.LogInformation ($"Permanently deleted {_selectedUser.Home} directory");
                        else
                            _logger.LogError ($"Error deleting {_selectedUser.Home}", result.ErrorMessage);
                    } catch (Exception ex) {
                        _logger.LogCritical ($"Error deleting {_selectedUser.Home} directory", ex);
                    }
                }
            }
        }
    }
}