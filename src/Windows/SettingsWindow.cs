//     This file is part of Faris System Accounts.
//     Faris System Accounts is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.

//     Faris System Accounts is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU General Public License for more details.

//     You should have received a copy of the GNU General Public License
//     along with Faris System Accounts.  If not, see<https://www.gnu.org/licenses/>.

using System;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;
using Faris.GnuSystem.Gui.Windows;
using Faris.GnuSystem.Services.Caching;
using Faris.SystemAccounts.App.Models;
using Microsoft.Extensions.Logging;

namespace Faris.SystemAccounts.App.Windows {
    public class SettingsWindow : Window {
        SettingsModel _settings;
        readonly ILogger<SettingsWindow> _logger;
        public SettingsWindow (ILogger<SettingsWindow> logger, ICachingService cacheService) {
            _logger = logger;
            _settings = new SettingsModel ();
        }
        public override async Task NavigatingFromAsync () {
            OutputParameter = _settings;
            await SaveSettingsFileAsync ();
        }

        public override async Task NavigatingToAsync () {
            await ReadSettingsFileAsync ();
        }

        private async Task ReadSettingsFileAsync () {
            try {
                var configDir = $"/home/{Environment.UserName}/.config/faris/system-accounts/";
                var accountSwitch = GetNativeObject<Gtk.Switch> ("_displayAllAccountsSwitch");
                var groupSwitch = GetNativeObject<Gtk.Switch> ("_displayAllGroupsSwitch");
                using (FileStream fs = File.OpenRead (System.IO.Path.Combine (configDir, "settings.json"))) {
                    _settings = await JsonSerializer.DeserializeAsync<SettingsModel> (fs);
                    accountSwitch.State = _settings.DisplayAccounts;
                    groupSwitch.State = _settings.DisplayGroups;
                    fs.Close ();
                    await Task.Delay (100);
                }
            } catch (Exception ex) {
                _logger.LogError (ex, "Error reading settings file.");
            }
        }
        private async Task SaveSettingsFileAsync () {
            var accountSwitch = GetNativeObject<Gtk.Switch> ("_displayAllAccountsSwitch");
            var groupSwitch = GetNativeObject<Gtk.Switch> ("_displayAllGroupsSwitch");
            _settings.DisplayAccounts = accountSwitch.State;
            _settings.DisplayGroups = groupSwitch.State;

            var configDir = $"/home/{Environment.UserName}/.config/faris/system-accounts/";
            try {
                using (FileStream fs = File.Create (System.IO.Path.Combine (configDir, "settings.json"))) {
                    await JsonSerializer.SerializeAsync (fs, _settings);
                    await fs.FlushAsync ();
                    fs.Close ();
                    await Task.Delay (100);
                }
                _logger.LogTrace ("Settings file saved successfully.");
            } catch (Exception ex) {
                _logger.LogError (ex, "Eerror saving settings file.");
            }
        }
    }
}