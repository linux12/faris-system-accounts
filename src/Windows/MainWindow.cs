//     This file is part of Faris System Accounts.
//     Faris System Accounts is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.

//     Faris System Accounts is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU General Public License for more details.

//     You should have received a copy of the GNU General Public License
//     along with Faris System Accounts.  If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.Json;
using System.Threading.Tasks;
using Faris.GnuSystem.Commands;
using Faris.GnuSystem.Gui.Controls.DataGrid;
using Faris.GnuSystem.Gui.Windows;
using Faris.SystemAccounts.App.Models;
using Faris.SystemAccounts.App.Services;
using Microsoft.Extensions.Logging;

namespace Faris.SystemAccounts.App.Windows {
    public class MainWindow : Window {
        SettingsModel _settings;
        readonly ISystemAccountService _systemAccountService;
        readonly ILogger<MainWindow> _logger;
        DataGridControl<SystemUser> _usersDataGrid;
        DataGridControl<SystemGroup> _groupsDataGrid;

        public MainWindow (ISystemAccountService systemAccountService, ILogger<MainWindow> logger) {
            _logger = logger;
            _systemAccountService = systemAccountService;
            Navigator.WindowClosed += OnWindowClosed;
        }

        private async void OnWindowClosed (object sender, WindowClosedArgs e) {
            if (e.WindowType == typeof (SettingsWindow)) {
                _settings = e.OutputParameter as SettingsModel;
                await RefreshAsync ();
            } else if (e.OutputParameter is bool && e.WindowType == typeof (GroupWindow)) {
                if ((bool) e.OutputParameter)
                    await RefreshAsync ();
            } else if (e.OutputParameter is bool && e.WindowType == typeof (AddAccountWindow)) {
                if ((bool) e.OutputParameter)
                    await RefreshAsync ();
            } else if (e.OutputParameter is bool && e.WindowType == typeof (AddGroupWindow)) {
                if ((bool) e.OutputParameter)
                    await RefreshAsync ();
            } else if (e.OutputParameter is bool && e.WindowType == typeof (UserWindow)) {
                if ((bool) e.OutputParameter)
                    await RefreshAsync ();
            }
        }

        public override Task NavigatingFromAsync () {
            _logger.LogTrace ("Application closed");
            Navigator.WindowClosed -= OnWindowClosed;
            return Task.CompletedTask;
        }

        public override async Task NavigatingToAsync () {
            _logger.LogTrace ("Application started");
            var aboutDialog = GetNativeObject<Gtk.AboutDialog> ("_aboutDialog");
            var aboutButton = GetNativeObject<Gtk.Button> ("_aboutButton");
            var popoverbutton = GetNativeObject<Gtk.Button> ("_popoverButton");
            var settingsPopover = GetNativeObject<Gtk.Popover> ("_settingsPopover");
            var addUserButton = GetNativeObject<Gtk.Button> ("_addUserButton");
            var addGroupButton = GetNativeObject<Gtk.Button> ("_addGroupButton");
            var settingsButton = GetNativeObject<Gtk.Button> ("_settingsButton");
            var logsButton = GetNativeObject<Gtk.Button> ("_logsButton");
            aboutDialog.DeleteEvent += (sender, args) => { args.RetVal = aboutDialog.HideOnDelete (); };
            await CreateConfigDirectory ();

            await ReadSettings ();

            InitializeDataGrid ();

            await RefreshAsync ();

            SetAboutDialogAppVersion (aboutDialog);

            popoverbutton.Clicked += (sender, args) => {
                settingsPopover.Popup ();
            };
            aboutButton.Clicked += (sender, args) => {
                // var aboutButton = GetNativeObject<Gtk.Button> ("_aboutButton");
                 aboutDialog.ShowNow ();
                // aboutDialog.Dispose ();
            };
            addUserButton.Clicked += async (sender, args) => {
                await Navigator.NavigateToAsync (typeof (AddAccountWindow));
            };
            addGroupButton.Clicked += async (sender, args) => {
                await Navigator.NavigateToAsync (typeof (AddGroupWindow));
            };
            logsButton.Clicked += async (sender, args) => {
                await Navigator.NavigateToAsync (typeof (LogsWindow));
            };
            settingsButton.Clicked += async (sender, args) => {
                await Navigator.NavigateToAsync (typeof (SettingsWindow));
            };

        }

        private void SetAboutDialogAppVersion (Gtk.AboutDialog aboutDialog) {
            FileVersionInfo version = System.Diagnostics.FileVersionInfo.GetVersionInfo (Assembly.GetExecutingAssembly ().Location);
            aboutDialog.Version = $"Version {version.FileMajorPart}.{version.FileMinorPart}.{version.FileBuildPart}";
        }

        private void InitializeDataGrid () {
            var scrolledWindow = GetNativeObject<Gtk.ScrolledWindow> ("_userScrolledWindow");
            var userSearchEntry = GetNativeObject<Gtk.Entry> ("_searchAccountLoginEntry");

            _usersDataGrid = new DataGridControl<SystemUser> (scrolledWindow, userSearchEntry);

            _usersDataGrid.AddColumn (new TextColumn ("User ID", "UserID"));
            _usersDataGrid.AddColumn (new TextColumn ("Full Name", "Name"));
            _usersDataGrid.AddColumn (new TextColumn ("Login", "Login", true));
            _usersDataGrid.AddColumn (new TextColumn ("Shell", "Shell"));
            _usersDataGrid.AddColumn (new TextColumn ("Home", "Home"));

            var gscrolledWindow = GetNativeObject<Gtk.ScrolledWindow> ("_groupScrolledWindow");
            var gSearchEntry = GetNativeObject<Gtk.Entry> ("_searchGroupNameEntry");

            _groupsDataGrid = new DataGridControl<SystemGroup> (gscrolledWindow, gSearchEntry);

            _groupsDataGrid.AddColumn (new TextColumn ("Group ID", "ID"));
            _groupsDataGrid.AddColumn (new TextColumn ("Group", "Name", true));

            _groupsDataGrid.SelectionChanged += (sender, args) => {
                Navigator.NavigateToAsync (typeof (GroupWindow), args.Selected);
            };
            _usersDataGrid.SelectionChanged += (sender, args) => {
                Navigator.NavigateToAsync (typeof (UserWindow), args.Selected);
            };
            _usersDataGrid.Show ();
            _groupsDataGrid.Show ();
        }

        private async Task RefreshAsync () {
            _logger.LogTrace ("Reading System Account information");

            try {
                await _systemAccountService.RefreshAsync ();

                IEnumerable<SystemUser> users = _systemAccountService.Users;
                IEnumerable<SystemGroup> groups = _systemAccountService.Groups;

                if (!_settings.DisplayAccounts)
                    users = _systemAccountService.Users.Where (x => x.UserID >= 1000 && x.UserID != 65534).OrderByDescending (x => x.UserID);

                if (!_settings.DisplayGroups)
                    groups = _systemAccountService.Groups.Where (x => x.ID >= 1000 && x.ID != 65534).OrderByDescending (x => x.ID);

                _usersDataGrid.Populate (users);
                _groupsDataGrid.Populate (groups);
            } catch (Exception exc) {
                _logger.LogError (exc, exc.Message);
            }
        }
        private async Task ReadSettings () {
            try {
                var configDir = $"/home/{Environment.UserName}/.config/faris/system-accounts/";
                using (FileStream fs = File.OpenRead (System.IO.Path.Combine (configDir, "settings.json"))) {
                    _settings = await JsonSerializer.DeserializeAsync<SettingsModel> (fs);
                    fs.Close ();
                    await Task.Delay (100);
                }
                _logger.LogTrace ("Reading settings file successfully.");
            } catch (Exception ex) {
                _settings = new SettingsModel ();
                _logger.LogError (ex, "Error reading settings file");
            }
        }
        private async Task CreateConfigDirectory () {
            var configDir = $"/home/{Environment.UserName}/.config/faris/system-accounts/";

            if (!Directory.Exists (configDir)) {
                var cmd = new SystemCommand ("mkdir");
                cmd.AppendArgument ("-p");
                cmd.AppendArgument (configDir);
                var result = await cmd.ExecuteAsync (false);
                if (result.HasErrors) {
                    ShowMessageDialog (Gtk.MessageType.Error, Gtk.ButtonsType.Close, "Critical error, cannot create config directory. Application will shutdown now");
                    Close ();
                }
            }
            if (!File.Exists (System.IO.Path.Combine (configDir, "settings.json"))) {
                using (FileStream fs = File.Create (System.IO.Path.Combine (configDir, "settings.json"))) {
                    await JsonSerializer.SerializeAsync (fs, new SettingsModel ());
                    await fs.FlushAsync ();
                    fs.Close ();
                    await Task.Delay (10);
                }
            }
        }
    }
}