//     This file is part of Faris System Accounts.
//     Faris System Accounts is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.

//     Faris System Accounts is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU General Public License for more details.

//     You should have received a copy of the GNU General Public License
//     along with Faris System Accounts.  If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Threading.Tasks;
using Faris.GnuSystem.Exceptions;
using Faris.GnuSystem.Gui.Windows;
using Faris.SystemAccounts.App.Services;
using Microsoft.Extensions.Logging;

namespace Faris.SystemAccounts.App.Windows {
    public class AddGroupWindow : Window {
        readonly ISystemAccountService _systemAccountService;
        readonly ILogger _logger;

        public AddGroupWindow (ISystemAccountService systemAccountService, ILogger<AddGroupWindow> logger) {
            _systemAccountService = systemAccountService;
            _logger = logger;
        }
        public override Task NavigatingFromAsync () {
            _logger.LogTrace ("Navigated From");
            return Task.CompletedTask;
        }

        public override Task NavigatingToAsync () {
            _logger.LogTrace ("Navigated To");

            var createButton = GetNativeObject<Gtk.Button> ("_createGroupButton");
            createButton.Clicked += async (sender, args) => {
                await CreateGroupAsync ();
            };
            return Task.CompletedTask;
        }
        private async Task CreateGroupAsync () {
            var groupNameEntry = GetNativeObject<Gtk.Entry> ("_groupNameEntry");
            try {
                if (string.IsNullOrEmpty (groupNameEntry.Text)) {
                    ShowMessageDialog (Gtk.MessageType.Error, Gtk.ButtonsType.Ok, "Group name must be non empty.");
                    return;
                }
                var created = await _systemAccountService.CreateNewGroupAsync (groupNameEntry.Text);
                if (created) {
                    ShowMessageDialog (Gtk.MessageType.Info, Gtk.ButtonsType.Ok, $"Group \"{groupNameEntry.Text}\" created successfully.");
                    OutputParameter = true;
                } else {
                    _logger.LogError ($"Error creating \"{groupNameEntry.Text}\" group name.");
                    ShowMessageDialog (Gtk.MessageType.Warning, Gtk.ButtonsType.Ok, $"Group \"{groupNameEntry.Text}\" already exist.");

                }
            } catch (SystemCommandException exc) {
                _logger.LogCritical (exc, $"Error creating \"{groupNameEntry.Text}\" group name.");
            }
        }
    }
}