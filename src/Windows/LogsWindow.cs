//     This file is part of Faris System Accounts.
//     Faris System Accounts is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.

//     Faris System Accounts is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU General Public License for more details.

//     You should have received a copy of the GNU General Public License
//     along with Faris System Accounts.  If not, see<https://www.gnu.org/licenses/>.

using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Faris.GnuSystem.Gui.Windows;

namespace Faris.SystemAccounts.App.Windows {
    public class LogsWindow : Window {
        public LogsWindow () {

        }
        public override Task NavigatingFromAsync () {
            return Task.CompletedTask;
        }

        public override Task NavigatingToAsync () {
            return ReadLogFileAsync ();
        }

        private async Task ReadLogFileAsync () {
            var logsPath = $"/home/{Environment.UserName}/.config/faris/system-accounts/Logs";
            if (Directory.Exists (logsPath)) {
                var files = Directory.GetFiles (logsPath);
                var lastLog = files.OrderByDescending (x => x).FirstOrDefault ();
                var text = await System.IO.File.ReadAllTextAsync (lastLog);
                var txtView = GetNativeObject<Gtk.TextView> ("_textView");
                txtView.Buffer.Text = text;
            }
        }
    }
}