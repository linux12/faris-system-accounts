//     This file is part of Faris System Accounts.
//     Faris System Accounts is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.

//     Faris System Accounts is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU General Public License for more details.

//     You should have received a copy of the GNU General Public License
//     along with Faris System Accounts.  If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Threading.Tasks;
using Faris.GnuSystem.Gui.Windows;
using Faris.SystemAccounts.App.Models;
using Faris.SystemAccounts.App.Services;
using Microsoft.Extensions.Logging;

namespace Faris.SystemAccounts.App.Windows {
    public class AddAccountWindow : Window {
        Gtk.ComboBoxText shellsComboBox;
        Gtk.CheckButton systemAccountCheckButton;
        Gtk.CheckButton systemHomeCheckButton;
        Gtk.Entry loginEntry;
        Gtk.Entry nameEntry;
        Gtk.Entry homeEntry;
        Gtk.Button resetButton;
        Gtk.Button addAccountButton;
        readonly ISystemAccountService _systemAccountService;
        readonly ILogger<AddAccountWindow> _logger;
        public AddAccountWindow (ISystemAccountService systemAccountService, ILogger<AddAccountWindow> addAccountLogger) {
            _systemAccountService = systemAccountService;
            _logger = addAccountLogger;
        }

        public override Task NavigatingFromAsync () {
            _logger.LogTrace ("Navigated From");
            return Task.CompletedTask;
        }
        private void GetNativeObjects () {
            systemAccountCheckButton = GetNativeObject<Gtk.CheckButton> ("_systemAccountCheckButton");
            systemHomeCheckButton = GetNativeObject<Gtk.CheckButton> ("_systemHomeCheckButton");
            loginEntry = GetNativeObject<Gtk.Entry> ("_loginEntry");
            homeEntry = GetNativeObject<Gtk.Entry> ("_homeEntry");
            resetButton = GetNativeObject<Gtk.Button> ("_resetEntriesButton");
            nameEntry = GetNativeObject<Gtk.Entry> ("_nameEntry");
            addAccountButton = GetNativeObject<Gtk.Button> ("_addUserButton");
            shellsComboBox = GetNativeObject<Gtk.ComboBoxText> ("_shellsComboBoxText");

        }
        public override async Task NavigatingToAsync () {
            _logger.LogTrace ("Navigated To");

            GetNativeObjects ();
            systemAccountCheckButton.Active = false;
            systemHomeCheckButton.Visible = false;
            resetButton.Clicked += (sender, args) => { ResetEntries (); };
            systemAccountCheckButton.Toggled += (sender, args) => { ToggleSystemAccount (); };
            systemHomeCheckButton.Toggled += (sender, args) => { ToggleHomeDirectory (); };
            addAccountButton.Clicked += async (sender, args) => { await AddAccountAsync (); };
            loginEntry.FocusOutEvent += (sender, args) => {
                homeEntry.Text = $"/home/{loginEntry.Text}";
                args.RetVal = true;
            };
            await PopulateShellsComboBox ();
        }
        private async Task PopulateShellsComboBox () {
            var systemShells = await _systemAccountService.GetSystemShellsAsync ();

            foreach (var item in systemShells) {
                shellsComboBox.AppendText (item);
            }
        }
        private void ResetEntries () {
            homeEntry.Text = string.Empty;
            loginEntry.Text = string.Empty;
            nameEntry.Text = string.Empty;
            systemAccountCheckButton.Active = false;
            systemHomeCheckButton.Active = false;
            shellsComboBox.Active = -1;
        }

        private void ToggleHomeDirectory () {
            if (systemHomeCheckButton.Active) {
                if (!string.IsNullOrWhiteSpace (loginEntry.Text))
                    homeEntry.Text = $"/home/{loginEntry.Text}";
                else
                    homeEntry.Text = "";
                homeEntry.IsEditable = true;
            } else {
                homeEntry.IsEditable = false;
                homeEntry.Text = "/nonexistent";
            }
        }

        private void ToggleSystemAccount () {
            if (systemAccountCheckButton.Active) {
                shellsComboBox.Parent.Visible = false;
                shellsComboBox.Active = -1;
                systemHomeCheckButton.Visible = true;
                systemHomeCheckButton.Parent.Visible = true;
                homeEntry.IsEditable = false;
                homeEntry.Text = "/nonexistent";
            } else {
                shellsComboBox.Parent.Visible = true;
                systemHomeCheckButton.Visible = false;
                systemHomeCheckButton.Active = false;
                homeEntry.IsEditable = true;
                if (!string.IsNullOrWhiteSpace (loginEntry.Text))
                    homeEntry.Text = $"/home/{loginEntry.Text}";
                else
                    homeEntry.Text = "";
            }
        }
        private async Task AddAccountAsync () {
            if (string.IsNullOrEmpty (loginEntry.Text)) {
                ShowMessageDialog (Gtk.MessageType.Error, Gtk.ButtonsType.Ok, "Login name should not be empty");
                return;
            }

            if (string.IsNullOrEmpty (shellsComboBox.ActiveText) && !systemAccountCheckButton.Active) {
                ShowMessageDialog (Gtk.MessageType.Error, Gtk.ButtonsType.Ok, "You must specify a login shell");
                return;
            }

            SystemUser user = new SystemUser ();
            user.IsSystem = systemAccountCheckButton.Active;
            user.IsSystemHome = systemHomeCheckButton.Active;
            user.Home = homeEntry.Text;
            user.Login = loginEntry.Text.ToLower ();
            user.Name = nameEntry.Text;

            if (user.IsSystem) {
                user.Shell = "/usr/sbin/nologin";
            } else {
                user.Shell = shellsComboBox.ActiveText;
            }

            try {
                var created = await _systemAccountService.CreateNewAccountAsync (user);
                if (created) {
                    _logger.LogInformation ($"User \"{user.Login}\" created successfully");
                    OutputParameter = true;
                    ResetEntries ();
                    ShowMessageDialog (Gtk.MessageType.Info, Gtk.ButtonsType.Ok, "Account created successfully.");
                }
            } catch (Exception exc) {
                _logger.LogError (exc, "Error creating new account");
            }
        }
    }
}