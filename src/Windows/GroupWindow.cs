//     This file is part of Faris System Accounts.
//     Faris System Accounts is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.

//     Faris System Accounts is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU General Public License for more details.

//     You should have received a copy of the GNU General Public License
//     along with Faris System Accounts.  If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Threading.Tasks;
using Faris.GnuSystem.Exceptions;
using Faris.GnuSystem.Gui.Windows;
using Faris.SystemAccounts.App.Models;
using Faris.SystemAccounts.App.Services;
using Microsoft.Extensions.Logging;

namespace Faris.SystemAccounts.App.Windows {
    public class GroupWindow : Window {
        readonly ISystemAccountService _systemAccountService;
        readonly ILogger _logger;
        SystemGroup _selectedGroup;
        Gtk.Entry _groupNameEntry;

        public GroupWindow (ISystemAccountService systemAccountService, ILogger<GroupWindow> logger) {
            _logger = logger;
            _systemAccountService = systemAccountService;
        }
        public override Task NavigatingFromAsync () {
            _logger.LogTrace ("Navigating From");
            return Task.CompletedTask;
        }

        public override Task NavigatingToAsync () {
            _logger.LogTrace ("Navigating To");
            _selectedGroup = InputParameter as SystemGroup;
            if (_selectedGroup == null)
                return Task.CompletedTask;
            OutputParameter = false;
            _groupNameEntry = GetNativeObject<Gtk.Entry> ("_groupNameEntry");

            var deleteButton = GetNativeObject<Gtk.Button> ("_deleteGroupButton");
            var renameButton = GetNativeObject<Gtk.Button> ("_renameGroupButton");

            _groupNameEntry.Text = _selectedGroup.Name;

            deleteButton.Clicked += async (sender, args) => { await DeleteGroupAsync (); };
            renameButton.Clicked += async (sender, args) => { await RenameGroupAsync (); };

            return Task.CompletedTask;
        }

        private async Task DeleteGroupAsync () {
            try {
                var dialogResponse = ShowMessageDialog (Gtk.MessageType.Question, Gtk.ButtonsType.YesNo, $"Are you sure you want to delete \"{_groupNameEntry.Text}\" group ?");
                if (dialogResponse == Gtk.ResponseType.Yes) {
                    var deleted = await _systemAccountService.DeleteGroupAsync (_selectedGroup.Name);
                    if (deleted) {
                        OutputParameter = true;
                        ShowMessageDialog (Gtk.MessageType.Info, Gtk.ButtonsType.Ok, "Group deleted successfully");
                        Close ();
                    }
                }
            } catch (SystemCommandException ex) {
                _logger.LogError ("Error deleting group", ex);
            } catch (Exception ex) {
                _logger.LogCritical ("Error deleting group", ex);
            }
        }

        private async Task RenameGroupAsync () {
            try {
                var dialogResponse = ShowMessageDialog (Gtk.MessageType.Question, Gtk.ButtonsType.YesNo, $"Are you sure you want to rename the group to\"{_groupNameEntry.Text}\" ?");
                if (dialogResponse == Gtk.ResponseType.Yes) {
                    var renamed = await _systemAccountService.RenameGroupAsync (_selectedGroup, _groupNameEntry.Text);
                    if (renamed) {
                        OutputParameter = true;
                        ShowMessageDialog (Gtk.MessageType.Info, Gtk.ButtonsType.Ok, "Group renamed successfully");
                        Close ();
                    }
                }
            } catch (Exception ex) {
                _logger.LogCritical ($"Error renaming group to {_groupNameEntry.Text}", ex);
            }
        }
    }
}