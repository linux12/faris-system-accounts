//     This file is part of Faris System Accounts.
//     Faris System Accounts is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.

//     Faris System Accounts is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU General Public License for more details.

//     You should have received a copy of the GNU General Public License
//     along with Faris System Accounts.  If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Faris.GnuSystem.Commands;
using Faris.GnuSystem.Parsers;
using Faris.SystemAccounts.App.Models;
using Faris.SystemAccounts.App.Parsers;

namespace Faris.SystemAccounts.App.Services {
    public class SystemUserService : ISystemUserService {
        private IFileParser<IList<SystemUser>> _usersParser;

        public SystemUserService () {
            _usersParser = new UsersParser ();
        }
        public Task<IList<SystemUser>> GetUsersAsync () {
            var lstUsers = _usersParser.ParseContentAsync ("/etc/passwd");
            return lstUsers;
        }

        public async Task<bool> CreateUserAsync (SystemUser user) {
            if (user == null)
                throw new ArgumentNullException (nameof (user));

            ISystemCommand userAddCommand = new SystemCommand ("useradd");

            CreateArgumentsForNewUser (userAddCommand, user);

            var result = await userAddCommand.ExecuteAsync (true);
            return result.ExitCode == 0;
        }

        public async Task<bool> DeleteUserAsync (string login) {
            if (string.IsNullOrWhiteSpace (login))
                throw new ArgumentException ("Must be not null or empty", nameof (login));

            ISystemCommand delUserCommand = new SystemCommand ("userdel");

            delUserCommand.AppendArgument (login);

            var result = await delUserCommand.ExecuteAsync (true);
            return result.ExitCode == 0;
        }

        public async Task<bool> ChangePasswordAsync (string login, string password) {
            if (string.IsNullOrWhiteSpace (login))
                throw new ArgumentException ("Must be not null or empty", nameof (login));
            if (string.IsNullOrWhiteSpace (password))
                throw new ArgumentException ("Must be not null or empty", nameof (password));

            ISystemCommand passwdCommand = new SystemCommand ("passwd");

            passwdCommand.AppendArgument (login);
            string[] inputs = new string[2];
            inputs[0] = inputs[1] = password;
            var result = await passwdCommand.ExecuteAsync (true, inputs);
            return result.ExitCode == 0;
        }

        public async Task<bool> ChangeUserAsync (SystemUser user, SystemUser modifiedUser) {
            if (user == null)
                throw new ArgumentNullException (nameof (user));
            if (modifiedUser == null)
                throw new ArgumentNullException (nameof (modifiedUser));

            ISystemCommand userAddCommand = new SystemCommand ("usermod");

            bool changed = CreateArgumentsForUserModification (userAddCommand, user, modifiedUser);
            if (changed) {
                var result = await userAddCommand.ExecuteAsync (true);
                return result.ExitCode == 0;
            } else {
                return false;
            }
        }
        public async Task<bool> AddUserGroupsAsync (SystemUser user, params string[] groups) {
            if (user == null)
                throw new ArgumentNullException (nameof (user));
            if (groups.Length == 0)
                throw new ArgumentException ("groups parameter must have at least 1 group");

            ISystemCommand userModCommand = new SystemCommand ("usermod");

            var builder = new StringBuilder ();
            builder.AppendJoin (",", groups);

            userModCommand.AppendArgument ($"-a -G {builder.ToString()} {user.Login}");

            var result = await userModCommand.ExecuteAsync (true);
            return result.ExitCode == 0;
        }
        public async Task<bool> SetUserGroupsAsync (SystemUser user, params string[] groups) {
            if (user == null)
                throw new ArgumentNullException (nameof (user));
            if (groups.Length == 0)
                throw new ArgumentException ("groups parameter must have at least 1 group");

            ISystemCommand userModCommand = new SystemCommand ("usermod");

            var builder = new StringBuilder ();
            builder.AppendJoin (",", groups);

            userModCommand.AppendArgument ($"-G {builder.ToString()} {user.Login}");

            var result = await userModCommand.ExecuteAsync (true);
            return result.ExitCode == 0;

        }
        private bool CreateArgumentsForUserModification (ISystemCommand command, SystemUser user, SystemUser modifiedUser) {
            bool changed = false;

            if (!user.Home.Equals (modifiedUser.Home) && !string.IsNullOrEmpty (modifiedUser.Home)) {
                command.AppendArgument ($"-m -d {modifiedUser.Home}");
                changed = true;
            }

            if (!user.Login.Equals (modifiedUser.Login) && !string.IsNullOrEmpty (modifiedUser.Login)) {
                command.AppendArgument ($"-l {modifiedUser.Login}");
                changed = true;
            }

            if (!user.Shell.Equals (modifiedUser.Shell) && !string.IsNullOrEmpty (modifiedUser.Shell)) {
                command.AppendArgument ($"-s {modifiedUser.Shell}");
                changed = true;
            }

            if (!user.Name.Equals (modifiedUser.Name)) {
                command.AppendArgument ($"-c \"{modifiedUser.Name}\" ");
                changed = true;
            }
            if (user.UserID != modifiedUser.UserID) {
                command.AppendArgument ($"-u {modifiedUser.UserID}");
                changed = true;
            }
            if (user.GroupID != modifiedUser.GroupID) {
                command.AppendArgument ($"-g {modifiedUser.GroupID}");
                changed = true;
            }
            if (changed) {
                command.AppendArgument ($"{user.Login}");
            }

            return changed;
        }
        private void CreateArgumentsForNewUser (ISystemCommand command, SystemUser user) {
            if (user.IsSystem)
                command.AppendArgument ("-r");

            if (!string.IsNullOrEmpty (user.Home)) {
                command.AppendArgument ($"-m -d {user.Home}");
            }

            if (!string.IsNullOrEmpty (user.Shell)) {
                command.AppendArgument ($"-s {user.Shell}");
            }

            if (!string.IsNullOrEmpty (user.Name))
                command.AppendArgument ($"-c \"{user.Name}\"");

            if (user.UserID != 0)
                command.AppendArgument ($"-u {user.UserID.ToString()}");

            if (!string.IsNullOrEmpty (user.Login))
                command.AppendArgument ($"{user.Login}"); 
        }
    }
}