//     This file is part of Faris System Accounts.
//     Faris System Accounts is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.

//     Faris System Accounts is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU General Public License for more details.

//     You should have received a copy of the GNU General Public License
//     along with Faris System Accounts.  If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Faris.SystemAccounts.App.Models;

namespace Faris.SystemAccounts.App.Services
{
    public interface ISystemUserService
    {
         Task<IList<SystemUser>> GetUsersAsync();

        Task<bool> CreateUserAsync(SystemUser user);

        Task<bool> DeleteUserAsync(string login);

        Task<bool> ChangePasswordAsync(string login, string password);

        Task<bool> ChangeUserAsync(SystemUser user, SystemUser modifiedUser);

        Task<bool> SetUserGroupsAsync(SystemUser user, params string[] groups);

        Task<bool> AddUserGroupsAsync(SystemUser user, params string[] groups);
    }
}
