//     This file is part of Faris System Accounts.
//     Faris System Accounts is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.

//     Faris System Accounts is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU General Public License for more details.

//     You should have received a copy of the GNU General Public License
//     along with Faris System Accounts.  If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Faris.GnuSystem.Commands;
using Faris.GnuSystem.Parsers;
using Faris.SystemAccounts.App.Models;
using Faris.SystemAccounts.App.Parsers;

namespace Faris.SystemAccounts.App.Services {
    public class SystemGroupService : ISystemGroupService {
        private IFileParser<IList<SystemGroup>> _groupsParser;
        public SystemGroupService () {
            _groupsParser = new GroupsParser ();
        }

        public Task<IList<SystemGroup>> GetGroupsAsync () {
            var lstGroups = _groupsParser.ParseContentAsync ("/etc/group");
            return lstGroups;
        }

        public async Task<bool> CreateGroupAsync (string groupName) {
            if (string.IsNullOrWhiteSpace (groupName))
                throw new ArgumentException ("Must be not null or empty", nameof (groupName));

            ISystemCommand groupAddCmd = new SystemCommand ("groupadd");
            groupAddCmd.AppendArgument (groupName);
            var result = await groupAddCmd.ExecuteAsync (true);
            return result.ExitCode == 0;
        }

        public async Task<bool> DeleteGroupAsync (string groupName) {
            if (string.IsNullOrWhiteSpace (groupName))
                throw new ArgumentException ("Must be not null or empty", nameof (groupName));

            ISystemCommand cmd = new SystemCommand ("groupdel");
            cmd.AppendArgument (groupName);
            var result = await cmd.ExecuteAsync (true);
            return result.ExitCode == 0;

        }
        public async Task<List<string>> GetUserGroups (string userLogin) {

            ISystemCommand cmd = new SystemCommand ("id");
            cmd.AppendArgument ("-nG");
            cmd.AppendArgument (userLogin);
            var result = await cmd.ExecuteAsync (false);

            var lstGroups = new List<string> ();
            if (result.ExitCode == 0) {
                var groupsArray = result.Result.Replace (Environment.NewLine, "").Split (' ');
                lstGroups.AddRange (groupsArray);
            }
            return lstGroups;
        }
        public async Task<bool> RenameGroupAsync (string oldName, string newName) {
            ISystemCommand cmd = new SystemCommand ("groupmod");
            cmd.AppendArgument ("-n");
            cmd.AppendArgument (newName);
            cmd.AppendArgument (oldName);

            var result = await cmd.ExecuteAsync (true);

            return result.ExitCode == 0;
        }
    }
}