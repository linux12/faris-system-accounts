//     This file is part of Faris System Accounts.
//     Faris System Accounts is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.

//     Faris System Accounts is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU General Public License for more details.

//     You should have received a copy of the GNU General Public License
//     along with Faris System Accounts.  If not, see<https://www.gnu.org/licenses/>.

using System;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;
using Faris.GnuSystem.Commands;
using Faris.SystemAccounts.App.Models;

namespace Faris.SystemAccounts.App.Services {
    public class UserProfileService : IUserProfileService {

        public async Task<bool> ExportBashFilesAsync (SystemUser user, string dstPath) {
            if (user == null)
                throw new ArgumentNullException (nameof (user));
            if (string.IsNullOrEmpty (dstPath))
                throw new ArgumentNullException (nameof (dstPath));

            DirectoryInfo tmpDirectory = Directory.CreateDirectory ($"/tmp/{user.Login}-profile");

            string dstArchiveFilename = $"{user.Login}-profile.zip";

            string bashrcPath = System.IO.Path.Combine (user.Home, ".bashrc");
            string profilePath = System.IO.Path.Combine (user.Home, ".profile");
            string bashLogoutPath = System.IO.Path.Combine (user.Home, ".bash_logout"); 

            ISystemCommand cpCommand = new SystemCommand ("cp");

            cpCommand.AppendArgument (bashrcPath);
            cpCommand.AppendArgument (profilePath); 
            cpCommand.AppendArgument (bashLogoutPath);
            cpCommand.AppendArgument (tmpDirectory.FullName);
            try {
                var result = await cpCommand.ExecuteAsync (false);

                if (result.ExitCode == 0) {
                    var dstArchivePath = System.IO.Path.Combine (dstPath, dstArchiveFilename);
                    ZipFile.CreateFromDirectory (tmpDirectory.FullName, dstArchivePath);
                    return true;
                }
                return false;
            } catch (Exception ex) { throw ex; } finally {
                try {
                    ISystemCommand rmCmd = new SystemCommand ("rm");
                    rmCmd.AppendArgument ("-rf");
                    rmCmd.AppendArgument (tmpDirectory.FullName);
                    await rmCmd.ExecuteAsync (false);
                } catch (Exception ex) { throw ex; }
            }
        }

        public async Task<bool> ExportPgpKeysAsync (SystemUser user, string dstPath) {
            if (user == null)
                throw new ArgumentNullException (nameof (user));
            if (string.IsNullOrEmpty (dstPath))
                throw new ArgumentNullException (nameof (dstPath));

            string dstArchiveFilename = $"{user.Login}-gnupg-keys.zip";
            var srcPath = System.IO.Path.Combine (user.Home, ".gnupg");

            DirectoryInfo tmpDirectory = Directory.CreateDirectory ($"/tmp/{user.Login}-gnupg-keys");

            ISystemCommand cpCommand = new SystemCommand ("cp");

            cpCommand.AppendArgument ("-r");
            cpCommand.AppendArgument (srcPath);
            cpCommand.AppendArgument (tmpDirectory.FullName);

            try {
                var result = await cpCommand.ExecuteAsync (false);

                if (result.ExitCode == 0) {
                    var dstArchivePath = System.IO.Path.Combine (dstPath, dstArchiveFilename);
                    ZipFile.CreateFromDirectory (tmpDirectory.FullName, dstArchivePath);
                    return true;
                }
                return false;
            } catch (Exception ex) { throw ex; } finally {
                try {
                    ISystemCommand rmCmd = new SystemCommand ("rm");
                    rmCmd.AppendArgument ("-rf");
                    rmCmd.AppendArgument (tmpDirectory.FullName);
                    await rmCmd.ExecuteAsync (false);
                } catch (Exception ex) { throw ex; }
            }
        }

        public async Task<bool> ExportSshKeysAsync (SystemUser user, string dstPath) {
            if (user == null)
                throw new ArgumentNullException (nameof (user));
            if (string.IsNullOrEmpty (dstPath))
                throw new ArgumentNullException (nameof (dstPath));

            string dstArchiveFilename = $"{user.Login}-ssh-keys.zip";
            var srcPath = System.IO.Path.Combine (user.Home, ".ssh");

            DirectoryInfo tmpDirectory = Directory.CreateDirectory ($"/tmp/{user.Login}-ssh-keys");

            ISystemCommand cpCommand = new SystemCommand ("cp");

            cpCommand.AppendArgument ("-r");
            cpCommand.AppendArgument (srcPath);
            cpCommand.AppendArgument (tmpDirectory.FullName);

            try {
                var result = await cpCommand.ExecuteAsync (false);

                if (result.ExitCode == 0) {
                    var dstArchivePath = System.IO.Path.Combine (dstPath, dstArchiveFilename);
                    ZipFile.CreateFromDirectory (tmpDirectory.FullName, dstArchivePath);
                    return true;
                }
                return false;
            } catch (Exception ex) { throw ex; } finally {
                try {
                    ISystemCommand rmCmd = new SystemCommand ("rm");
                    rmCmd.AppendArgument ("-rf");
                    rmCmd.AppendArgument (tmpDirectory.FullName);
                    await rmCmd.ExecuteAsync (false);
                } catch (Exception ex) { throw ex; }
            }
        }
    }
}