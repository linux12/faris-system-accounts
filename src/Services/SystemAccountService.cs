//     This file is part of Faris System Accounts.
//     Faris System Accounts is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.

//     Faris System Accounts is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU General Public License for more details.

//     You should have received a copy of the GNU General Public License
//     along with Faris System Accounts.  If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faris.GnuSystem.Commands;
using Faris.SystemAccounts.App.Models;
using Faris.SystemAccounts.App.Parsers;

namespace Faris.SystemAccounts.App.Services {
    public class SystemAccountService : ISystemAccountService {

        private ISystemUserService _userService;
        private ISystemGroupService _groupService;
        public List<SystemGroup> Groups { get; private set; }
        public List<SystemUser> Users { get; private set; }
        public SystemAccountService (ISystemUserService userService, ISystemGroupService groupService) {
            _userService = userService;
            _groupService = groupService;
            Users = new List<SystemUser> ();
            Groups = new List<SystemGroup> ();
        }
        public async Task RefreshAsync () {
            Groups.Clear ();
            Users.Clear ();

            var lstUsers = _userService.GetUsersAsync ();
            var lstGroups = _groupService.GetGroupsAsync ();

            await Task.WhenAll (lstUsers, lstGroups);

            Users = lstUsers.Result.OrderByDescending (x => x.UserID).ToList ();
            Groups = lstGroups.Result.OrderByDescending (x => x.ID).ToList ();
        }

        public Task<bool> CreateNewAccountAsync (SystemUser user) {
            if (user == null)
                throw new ArgumentNullException (nameof (user));

            return _userService.CreateUserAsync (user);
        }
        public Task<bool> CreateNewGroupAsync (string groupName) {
            return _groupService.CreateGroupAsync (groupName);
        }

        public Task<bool> ChangePasswordAsync (string login, string password) {
            return _userService.ChangePasswordAsync (login, password);
        }
        public Task<bool> ChangeUserAsync (SystemUser user, SystemUser modifiedUser) {
            return _userService.ChangeUserAsync (user, modifiedUser);
        }
        public Task<List<string>> GetUserGroups (string user) {
            return _groupService.GetUserGroups (user);
        }
        public Task<IList<SystemGroup>> GetGroupsAsync () {
            return _groupService.GetGroupsAsync ();
        }
        public Task SetUserGroupsAsync (SystemUser user, List<string> groups) {
            return _userService.SetUserGroupsAsync (user, groups.ToArray ());
        }
        public Task<bool> DeleteUserAsync (SystemUser user) {
            return _userService.DeleteUserAsync (user.Login);
        }
        public Task<bool> DeleteGroupAsync (string groupName) {
            return _groupService.DeleteGroupAsync (groupName);
        }

        public async Task<IEnumerable<string>> GetSystemShellsAsync () {
            var parser = new ShellParser ();

            var shells = new List<string> ();

            shells.Add ("/bin/false");
            shells.Add ("/usr/sbin/nologin");

            var availableShells = await parser.ParseContentAsync ("/etc/shells");

            shells.AddRange (availableShells);

            return shells;
        }

        public Task<bool> RenameGroupAsync (SystemGroup group, string newName) {
            return _groupService.RenameGroupAsync (group.Name, newName);
        }
    }
}